class User < ActiveRecord::Base
  has_many :comments, dependent: :destroy
  has_many :posts

  has_secure_password

  validates :name, presence: true
  validates :email, presence: true, format: /\A\S+@\S+\z/, uniqueness: { case_sensitive: false }
  validates :password, length: { minimum: 6, allow_blank: true }

  def gravatar_id
  	Digest::MD5::hexdigest(email.downcase)
  end
end
