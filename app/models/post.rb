class Post < ActiveRecord::Base

	has_many :comments, dependent: :destroy
	belongs_to :user

	validates :title, :body, presence: true

	def self.all_descending
		where("created_at <= ?", Time.now).order("created_at desc")
	end
end
