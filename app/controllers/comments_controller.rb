class CommentsController < ApplicationController

	before_action :set_post
	before_action :require_signin
	before_action :require_admin, only: [:destroy]

	def new
		@comment = @post.comments.new
	end

	def create
		@comment = @post.comments.new(comment_params)
		@comment.user = current_user
		if @comment.save
			redirect_to @post, notice: "Comment suceessfully posted!"
		else
			render :new
	end

	def destroy
	end
end

private

	def set_post
		@post = Post.find(params[:post_id])
	end

	def comment_params
		params.require(:comment).permit(:comment_title, :comment_body)
	end
end
