class SessionsController < ApplicationController

  def new
  end

  def create
  	user = User.find_by(email: params[:email])
  	if user && user.authenticate(params[:password])
  		session[:user_id] = user.id
  		flash[:notice] = "Welcome back, #{user.name}!"
  		redirect_to user
  	else
  		flash.now[:warning] = "Invalid email and/or password"
  		render :new
  	end
  end

  def destroy
  	session[:user_id] = nil
  	redirect_to root_url, notice: "You've signed out!"
  end
end
