class UsersController < ApplicationController

	before_action :require_signin, except: [:new, :create]
	before_action :require_correct_user, only: [:show, :edit, :update, :destroy]

	def show
		set_user
	end

	def new
		@user = User.new
	end

	def create
		@user = User.new(user_params)
		if @user.save
			session[:user_id] = @user.id
			redirect_to @user, notice: "New user account successfully created!"
		else
			render 'new'
		end
	end

	def edit
		set_user
	end

	def update
		set_user
		if @user.update(user_params)
			redirect_to @user, notice: "User account successfully updated!"
		else
			render 'edit'
		end
	end

	def destroy
		set_user
		@user.destroy
		session[:user_id] = nil
		redirect_to root_url, alert: "Account successfully deleted!"
	end

private

def set_user
	@user = User.find(params[:id])
end

def user_params
	params.require(:user).permit(:name, :email, :password, :password_confirmation)
end

def require_correct_user
	set_user
	unless current_user == @user
		redirect_to root_path
	end
end

end
