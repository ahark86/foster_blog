class PostsController < ApplicationController

	before_action :require_signin, except: [:index, :show]
	before_action :require_admin, except: [:index, :show]

	def index
		@posts = Post.all_descending
	end

	def show
		set_post
	end

	def edit
		set_post
	end

	def update
		set_post
		if @post.update(post_params)
			redirect_to @post, notice: "Blog post successfully updated!"
		else
			render :edit
		end
	end

	def new
		@post = Post.new
	end

	def create
		@post = Post.new(post_params)
		@post.user = current_user
		if @post.save
			redirect_to @post, notice: "Blog post successfully created!"
		else
			render :new
		end
	end

	def destroy
		set_post
		@post.destroy
		flash[:alert] = "Blog post destroyed!"
		redirect_to posts_url
	end

private

	def set_post
		@post = Post.find(params[:id])
	end

	def post_params
		params.require(:post).permit(:title, :body)
	end
end