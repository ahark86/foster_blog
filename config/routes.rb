Rails.application.routes.draw do
 
  get 'sessions/new'

  resources :users
  resources :posts do
  	resources :comments
  end
  resource :session

  root 'posts#index'
  get 'signup' => 'users#new'
  get 'signin' => 'sessions#new'
end
